#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/main.py
# Fares Fraij
# ---------------------------

from flask import render_template, jsonify
from create_db import app, db, Book, create_books
import subprocess

#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]

# ------------
# index
# ------------
@app.route('/')
def index():
	return render_template('hello.html')

# ------------
# book
# ------------	
@app.route('/books/')
def book():
	book_list = db.session.query(Book).all()
	return render_template('books.html', book_list = book_list)

# ------------
# bookjson
# ------------	
@app.route('/books/json/')
def bookjson():
    """
    turn your books of objects into a list of serializable values
    """
    books = db.session.query(Book).all()
    return jsonify([e.serialize() for e in books])

if __name__ == "__main__":
	app.run(host='0.0.0.0')
